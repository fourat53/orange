#!/bin/bash

set -e

host="$1"
port="$2"
shift
shift
cmd="$@"
timeout="${WAIT_FOR_TIMEOUT:-30}"

start_time=$(date +%s)

until [ "$(($(date +%s) - start_time))" -ge "$timeout" ]; do
  if nc -z -v -w1 "$host" "$port" 2>/dev/null; then
    >&2 echo "Database is up - executing command"
    exec $cmd
  fi
  echo "Waiting for the database to become available..."
  sleep 1
done

>&2 echo "Timeout reached. Database did not become available."
exit 1
