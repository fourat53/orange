FROM python:3.11.4

ENV PATH="./venv/Scripts:${PATH}"

RUN mkdir /backend
WORKDIR /backend
ADD . /backend/

RUN apt-get update

RUN apt-get install -y git
RUN git init

RUN apt-get install -y gcc python3-dev
RUN apt-get install -y libxml2-dev libxslt1-dev build-essential python3-lxml zlib1g-dev
RUN apt-get install -y default-mysql-client default-libmysqlclient-dev
RUN wget https://bootstrap.pypa.io/get-pip.py 
RUN  python3 get-pip.py
RUN rm get-pip.py

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY ./venv/Scripts venv/Scripts
RUN chmod +x ./*

RUN chmod -R 755 /backend

EXPOSE 8000

CMD ["./venv/Scripts/entrypoint.sh"]