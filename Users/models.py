from django.db import models
from django.contrib.auth.models import AbstractUser, Group, Permission
# from django.contrib.admin.models import LogEntry
# from django.contrib.sessions.models import Session
# from django.contrib.contenttypes.models import ContentType
# from Orange import settings


class User(AbstractUser):
    GENDER_CHOICES = [
        ('Homme', 'Homme'),
        ('Femme', 'Femme'),
    ]
    gender = models.CharField(max_length=6, choices=GENDER_CHOICES, default='Homme')
    age = models.PositiveIntegerField(null=True)
    number = models.PositiveIntegerField(null=True)
    cin = models.PositiveIntegerField(null=True,unique=True)
    REGION_CHOICES = [
        ('Ariana', 'Ariana'),
        ('Béja', 'Béja'),
        ('Ben Arous', 'Ben Arous'),
        ('Bizerte', 'Bizerte'),
        ('Gabès', 'Gabès'),
        ('Gafsa', 'Gafsa'),
        ('Jendouba', 'Jendouba'),
        ('Kairouan', 'Kairouan'),
        ('Kasserine', 'Kasserine'),
        ('Kébili', 'Kébili'),
        ('Kef', 'Kef'),
        ('Mahdia', 'Mahdia'),
        ('Manouba', 'Manouba'),
        ('Médenine', 'Médenine'),
        ('Monastir', 'Monastir'),
        ('Nabeul', 'Nabeul'),
        ('Sfax', 'Sfax'),
        ('Sidi Bouzid', 'Sidi Bouzid'),
        ('Siliana', 'Siliana'),
        ('Sousse', 'Sousse'),
        ('Tataouine', 'Tataouine'),
        ('Tozeur', 'Tozeur'),
        ('Tunis', 'Tunis'),
        ('Zaghouan', 'Zaghouan'),
    ]
    region = models.CharField(max_length=255, choices=REGION_CHOICES, default='Tunis')
    address = models.CharField(max_length=255, null=True)
    imagePath = models.CharField(max_length=255, null=True)

    def __str__(self):
        return self.username

    groups = models.ManyToManyField(
        Group,
        verbose_name='User Groups',
        blank=True,
        related_name='users_user_groups'
    )
    
    permissions = models.ManyToManyField(
        Permission,
        verbose_name='User Permissions',
        blank=True,
        related_name='users_permission'  
    )
    class Meta:
        db_table = 'auth_user'
