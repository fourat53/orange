from django.db.models import Q
from django.http import HttpResponse
from .models import User, Group, Permission
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect
from django.contrib.auth.hashers import make_password
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.core.files.storage import FileSystemStorage


def CostomRegister(request):
    if request.method == 'POST':
        nom = request.POST['nom']
        prenom = request.POST['prenom']
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']
        region = request.POST['region']
        role = request.POST['role']
        image = request.FILES.get('image')
        user = User.objects.create_user(username=username, password=password, email=email)
        user.first_name = nom
        user.last_name = prenom
        user.region = region
        if role == 'Client':
            user.is_superuser=0
            user.is_staff=0
        if role == 'Personel':
            user.is_superuser=0
            user.is_staff = 1
        if role == 'Administrateur':
            user.is_superuser=1
            user.is_staff=1
        if image:
            fs = FileSystemStorage()
            filename = fs.save(image.name, image)
            user.imagePath = fs.url(filename)

        user.save()
        messages.success(request, 'Votre compte a été créer avec succés!')
        return redirect('login')   
    return render(request, 'registration/register.html')

def CostomLogin(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None and (user.is_superuser == 1 or user.is_staff == 1):
            login(request, user)
            messages.success(request, 'Vous avez connecté avec succés!')
            return redirect('dashboard')
        else:
            messages.error(request, 'Identifiants de connexion invalides. Veuillez réessayer.')
    return render(request, 'registration/login.html')

def CostomLogout(request):
    logout(request)
    messages.success(request, 'Vous avez déconnecté avec succés!')
    return redirect('login')

@login_required
def index(request):
    users = User.objects.all()
    return render(request, 'index.html', {'utilisateurs': users})

@login_required
def dashboard(request):
    users = User.objects.all()
    countClients = User.objects.filter(Q(is_superuser=0) & Q(is_staff=0)).count()
    countPersonels = User.objects.filter(Q(is_superuser=0) & Q(is_staff=1)).count()
    countAdmins = User.objects.filter(Q(is_superuser=1)).count()
    countUsers = User.objects.all().count()
    percentageClients = countClients / countUsers * 100
    percentagePersonels = countPersonels / countUsers * 100
    percentageAdmins = countAdmins / countUsers * 100
    context = {
        'utilisateurs': users,
        'countClients': countClients,
        'countPersonels': countPersonels,
        'countAdmins': countAdmins,
        'countUsers': countUsers,
        'percentageClients': percentageClients,
        'percentagePersonels': percentagePersonels,
        'percentageAdmins': percentageAdmins,
    }
    return render(request, 'user/dashboard.html', context)

@login_required
def afficherEmployes(request):
    users = User.objects.filter(Q(is_superuser=1) | Q(is_staff=1))
    return render(request, 'user/afficherEmployes.html', {'utilisateurs': users})

@login_required
def afficherClients(request):
    users = User.objects.filter(Q(is_superuser=0) & Q(is_staff=0))
    return render(request, 'user/afficherClients.html', {'utilisateurs': users} )

@login_required
def profile(request, id):
    user = User.objects.get(pk=id)
    return render(request, 'user/profile.html', {'utilisateur': user})

# @login_required
# def ban(request, id):
#     user = User.objects.get(pk=id)
#     if user.is_active == 0 :
#         user.is_active == 1    
#     elif user.is_active == 1 :
#         user.is_active == 0    
#     user.save() 
#     return render(request, 'user/afficherEmployes.html', {'utilisateur': user})

# # First, get the group with the name 'Admin'
# admin_group = Group.objects.get(name='Admin')

# # Now, filter users who belong to the 'Admin' group
# users_in_admin_group = User.objects.filter(groups=admin_group)
