from django.urls import path
from . import views
from django.contrib.auth.views import LoginView,LogoutView

urlpatterns = [
    path('login/', views.CostomLogin, name='login'),
    path('logout/', views.CostomLogout, name='logout'),
    path('register/', views.CostomRegister, name='register'),
    path('dashboard/',views.dashboard, name='dashboard'),
    path('profile//<int:id>/',views.profile, name='profile'),
    path('afficherEmployes/',views.afficherEmployes, name='afficherEmployes'),
    path('afficherClients/',views.afficherClients, name='afficherClients'),
    path('index/',views.index, name='index'),
]